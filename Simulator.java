import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.Container;
import java.awt.event.WindowEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.RenderingHints;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.border.Border;
import javax.swing.BorderFactory;


class Bubble
{
    int x, y;
    double speed;
}


class Surface extends JPanel implements ActionListener
{
    GeneralPath star;
    List<Ellipse2D.Double> bubbles;
    List<Double> speed_y;
    private Timer timer;
    private int acc_grav = 9, diam = 10, col_dens = 20, visc = 20, sol_dens = 19, inside_val = 0;
    public static double average_velocity = 2.5;

    private final double points[][] = { 
        {0, Simulator.HEIGHT}, {400, Simulator.HEIGHT}, {250, Simulator.HEIGHT-300}, 
        {250, Simulator.HEIGHT-450}, {280, Simulator.HEIGHT-470}, {120, Simulator.HEIGHT-470},
        {150, Simulator.HEIGHT-450}, {150, Simulator.HEIGHT-300}
    };
    
    public Surface()
    {
        star = new GeneralPath();
        bubbles = new ArrayList<Ellipse2D.Double>();
        speed_y = new ArrayList<Double>();
        
        set_bubbles(20);
        initTimer(); 
    }

    public void reset()
    {
        for(int i=0; i<bubbles.size(); ++i)
        {
            double x = Math.random() * 340 + 30; 
            bubbles.get(i).setFrame(x, Simulator.HEIGHT-20, diam, diam);
        }
    }
    
    private double get_velocity()
    {
        double speed_tmp = (double)acc_grav * (double)diam * (double)diam;
        speed_tmp *= ((double)col_dens + (double)inside_val) - ((double)sol_dens - (double)inside_val);
        speed_tmp /= 18.0 * (double)visc;

        average_velocity = (double)Math.round(100.0 * speed_tmp) / 100.0;
        average_velocity = Math.min(average_velocity, 15.0);
        average_velocity = Math.max(average_velocity, 0.0);
         
        speed_tmp += Math.random() * 2;
        speed_tmp = Math.min(speed_tmp, 15.0);
        speed_tmp = Math.max(speed_tmp, 0.0);
        
        return speed_tmp;
    }

    public void set_bubbles(int val)
    {
        int len = bubbles.size();

        if(val > len)
        {
            for(int i=len; i<=val; ++i)
            {
                Ellipse2D.Double bubble_tmp = new Ellipse2D.Double(); 
                double speed_tmp;
                
                double x = Math.random() * 340 + 30; 
                bubble_tmp.setFrame(x, Simulator.HEIGHT-20, diam, diam);
                
                bubbles.add(bubble_tmp);
                speed_y.add(get_velocity());
            }
        }
        else
        {
            for(int i=len-1; i>=val; --i)
            {
                bubbles.remove(i);
                speed_y.remove(i);
            }
        }
    } 

    public void set_bubbles_1(int val)
    {
        inside_val = val;

        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }

    public void set_acc_grav(int val)
    {
        acc_grav = val;
        
        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }
    
    public void set_diam(int val)
    {
        diam = val;
        
        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }
    
    public void set_col_dens(int val)
    {
        col_dens = val;
        
        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }
    
    public void set_visc(int val)
    {
        visc = val;
        
        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }
    
    public void set_sol_dens(int val)
    {
        sol_dens = val;
        
        for(int i=0; i<bubbles.size(); ++i)
            speed_y.add(i, get_velocity());
    }
    
    private void initTimer()
    {
        timer = new Timer(30, this);
        timer.setInitialDelay(150);
        timer.start();
    }

    private void moveUp(int p)
    {
        if(bubbles.get(p).getY() > Simulator.HEIGHT-470+diam)
        {
            double new_x = bubbles.get(p).getX();
            double new_y = bubbles.get(p).getY() - speed_y.get(p);
            
            double line_right_x = (new_y - Simulator.HEIGHT + 800) / 2.07 - diam; 
            double line_left_x = (Simulator.HEIGHT - new_y) / 1.9; 
            
            if(new_y > Simulator.HEIGHT-300)
            {
                new_x = Math.min(new_x, line_right_x);
                new_x = Math.max(new_x, line_left_x);
            }

            bubbles.get(p).setFrame(new_x, new_y, diam, diam);
        }
        else
        {
            double new_x = bubbles.get(p).getX();

            if(new_x + diam > 250)
                new_x -= diam;
            
            else if(new_x - diam < 0)
                new_x += diam;    
            
            bubbles.get(p).setFrame(new_x, Simulator.HEIGHT-470+diam, diam, diam);
        }
    }
    
    private void drawBubbles(Graphics2D g2d)
    {
        for(int i=0; i<bubbles.size(); ++i)
        {
            g2d.setColor(new Color(209, 175, 165));
            moveUp(i);
            g2d.fill(bubbles.get(i));
        }
    }

    private void drawBottle(Graphics2D g2d)
    {
        star.moveTo(points[0][0], points[0][1]);

        for (int k=1; k<points.length; k++)
            star.lineTo(points[k][0], points[k][1]);

        star.closePath();
        g2d.fill(star);
    }
    
    private void doDrawing(Graphics g)
    {
        Graphics2D g2d = (Graphics2D) g.create();

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);

        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                             RenderingHints.VALUE_RENDER_QUALITY);

        g2d.setPaint(new Color(255, 99, 185));
        g2d.translate(100, -70);
        
        drawBottle(g2d); 
        drawBubbles(g2d);
        
        g2d.dispose();
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        doDrawing(g);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        repaint();
    }
}


public class Simulator extends JFrame
{
    public static int WIDTH = 900;
    public static int HEIGHT = 600;
    
    Container container = getContentPane();
    JPanel panel; 
    Surface surface;
    JSlider s_num_bubbles, s_num_1_bubbles, s_acc_grav, s_diam; 
    JSlider s_col_dens, s_visc, s_sol_dens;
    JButton reset_btn;
    JLabel avg_vel;

    public Simulator()
    { 
        JLabel l_num_bubbles = new JLabel("Number of Bubbles: ");
        JLabel l_num_1_bubbles = new JLabel("Number of Bubbles 2: ");
        JLabel l_acc_grav = new JLabel("Acceleration of Gravity: ");
        JLabel l_diam = new JLabel("E. coli Diameter: ");
        JLabel l_col_dens = new JLabel("E. coli Density: ");
        JLabel l_visc = new JLabel("Solution Viscosity: ");
        JLabel l_sol_dens = new JLabel("Solution Density: ");

        s_num_bubbles = new JSlider(JSlider.HORIZONTAL, 0, 1000, 20);
        s_num_1_bubbles = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);
        s_acc_grav = new JSlider(JSlider.HORIZONTAL, 1, 15, 9);
        s_diam = new JSlider(JSlider.HORIZONTAL, 0, 20, 10);
        s_col_dens = new JSlider(JSlider.HORIZONTAL, 0, 5000, 19);
        s_visc = new JSlider(JSlider.HORIZONTAL, 0, 1000, 20);
        s_sol_dens = new JSlider(JSlider.HORIZONTAL, 0, 5000, 20);
        add_sliders_listeners(); 
        
        reset_btn = new JButton("reset"); 
        add_buttons_listeners();

        avg_vel = new JLabel("Average Velocity = " + String.valueOf(Surface.average_velocity));        
        panel = new JPanel(new GridLayout(0, 1));

        Border emptyBorder = BorderFactory.createEmptyBorder(20, 20, 0, 0);
        panel.setBorder(emptyBorder);

        add_in_panel(l_num_bubbles, s_num_bubbles);
        add_in_panel(l_num_1_bubbles, s_num_1_bubbles);
        add_in_panel(l_acc_grav, s_acc_grav);
        add_in_panel(l_diam, s_diam);
        add_in_panel(l_col_dens, s_col_dens);
        add_in_panel(l_visc, s_visc);
        add_in_panel(l_sol_dens, s_sol_dens);
        
        panel.add(reset_btn); 
        panel.add(avg_vel);

        container.add(BorderLayout.WEST, panel);
        
        surface = new Surface();
        container.add(surface);

        setTitle("Simulator");
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void add_in_panel(JLabel lb, JSlider sl)
    {
        panel.add(lb);
        panel.add(sl);
    }
    
    private void add_sliders_listeners()
    {
        s_num_bubbles.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_bubbles(val);
                surface.repaint();
            }
        });
        
        s_num_1_bubbles.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_bubbles_1(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });
        
        s_acc_grav.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_acc_grav(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });

        s_diam.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_diam(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });
        
        s_col_dens.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_col_dens(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });
        
        s_visc.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_visc(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });
        
        s_sol_dens.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider tempSlider = (JSlider)e.getSource();
                int val = (int)tempSlider.getValue();
                surface.set_sol_dens(val);
                avg_vel.setText("Average Velocity = " + String.valueOf(Surface.average_velocity));
                surface.repaint();
            }
        });
    }

    private void add_buttons_listeners()
    {
        reset_btn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                surface.reset();
            }
        });
    }

    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                Simulator ex = new Simulator();
                ex.setVisible(true);
            }
        });
    }
}
